package com.example.zas.keyboard;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputType;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.crashlytics.android.Crashlytics;
import com.example.zas.keyboard.Objects.Employee;
import com.example.zas.keyboard.Objects.roleDeptGender;
import com.google.zxing.Result;

import io.fabric.sdk.android.Fabric;

import java.io.ByteArrayOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class Home extends AppCompatActivity implements ZXingScannerView.ResultHandler  {

    MyKeyboard keyboard;
    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    ImageView setting;
    ToggleButton toggleButton;
    EditText editText;
ImageView user;
    TextView tim, dat,scan,name,wel;
    ZXingScannerView qrCodeScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN
                , WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.home);
        sharedPreferences=getSharedPreferences("finger",MODE_PRIVATE);
        editor=sharedPreferences.edit();
if(sharedPreferences.getBoolean("first",true))
{
   IntitData();
}
        intite();

    }
private void Refresh()
    {
        editText.setText("");
        editText.setEnabled(true);
        wel.setText("Welcome");
        scan.setVisibility(View.VISIBLE);
        name.setVisibility(View.GONE);
        keyboard = (MyKeyboard) findViewById(R.id.keyboard);
        editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
        editText.setTextIsSelectable(true);
        InputConnection ic = editText.onCreateInputConnection(new EditorInfo());
        keyboard.setInputConnection(ic);
        user.setVisibility(View.GONE);
        qrCodeScanner.setVisibility(View.VISIBLE);
        qrCodeScanner.startCamera();
        // qrCodeScanner =(ZXingScannerView) findViewById(R.id.qrCodeScanner);

    }
    private void IntitData() {
        dataBaseHandler dataBaseHandler=new dataBaseHandler(this);
        String []department={"Web Development","Android Development","Human Resources","Adminstration","Public Relations"};
        int deptid=0;
        while (deptid<department.length)
        {
            deptid++;
            dataBaseHandler.addDepartment(new roleDeptGender(deptid,department[(deptid-1)]));
        }
        String []Gender={"Male","Female"};
        int genderid=0;
        while (genderid<Gender.length)
        {
            genderid++;
            dataBaseHandler.addGender(new roleDeptGender(genderid,Gender[(genderid-1)]));
        }
        String []Role={"Senior","Junior","Leader","Manager","Assistant"};
        int roleid=0;
        while (roleid<Role.length)
        {
            roleid++;
            dataBaseHandler.addRole(new roleDeptGender(roleid,Role[(roleid-1)]));
        }
        Drawable drawable=getResources().getDrawable(R.drawable.admin);
        Bitmap bitmap=((BitmapDrawable)drawable).getBitmap();
        ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
        bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
        byte[]buffer=byteArrayOutputStream.toByteArray();
        dataBaseHandler.addEmployee(new Employee("+20 1068123204","Admin","1000",2,2,"aa2580@fayoum.edu.eg","27/6/1995"
                ,2,"Fyoum",buffer,1));
        dataBaseHandler.close();
       editor=sharedPreferences.edit();
        editor.putBoolean("first",false);
        editor.putInt("lstpin",1000);
        editor.putInt("lstid",1);
        editor.commit();
        Toast.makeText(this,"2",Toast.LENGTH_LONG).show();
    }

    private void intite() {
        qrCodeScanner =(ZXingScannerView) findViewById(R.id.qrCodeScanner);
        if (ActivityCompat.checkSelfPermission(Home.this, Manifest.permission.CAMERA)
               != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA}, 200);


            //Show permission dialog
        } else {
            ImageView qr=(ImageView)findViewById(R.id.qr);
            qr.setImageResource(R.drawable.qr_active);
qrCodeScanner.setVisibility(View.VISIBLE);
           // qrCodeScanner =(ZXingScannerView) findViewById(R.id.qrCodeScanner);
            setScannerProperties();


        }
scan=(TextView)findViewById(R.id.scan);
        wel=(TextView)findViewById(R.id.wel);
        wel.setText("Welcome");
        scan.setVisibility(View.VISIBLE);
        user=(ImageView)findViewById(R.id.img);
        user.setVisibility(View.GONE);

        name=(TextView)findViewById(R.id.name);
        name.setVisibility(View.GONE);
        editText = (EditText) findViewById(R.id.editText);
       editText.setEnabled(true);
        editText.setText("");
        final ImageView imageView=(ImageView)findViewById(R.id.editimage);
        final int[] ima={R.drawable.empty,R.drawable.num1,R.drawable.num2,R.drawable.num3,R.drawable.num4};
        tim=(TextView)findViewById(R.id.tim);
         dat=(TextView)findViewById(R.id.dat);
       time();
         keyboard = (MyKeyboard) findViewById(R.id.keyboard);
        editText.setRawInputType(InputType.TYPE_CLASS_TEXT);
        editText.setTextIsSelectable(true);
        InputConnection ic = editText.onCreateInputConnection(new EditorInfo());
        keyboard.setInputConnection(ic);

        editor=sharedPreferences.edit();
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                int index=s.length();
                if(index==5) {
                    editText.setText("");

                }
                else
                imageView.setImageResource(ima[((index))]);
                if(index==4)
                {
                   // Refresh();
                    //Toast.makeText(Home.this,s.toString(),Toast.LENGTH_LONG).show();
                    checkPin(s.toString());

                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
         toggleButton=(ToggleButton)findViewById(R.id.toogle);
        toggleButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                intite();
            }
        });
        setting=(ImageView)findViewById(R.id.setting);
        setting.setVisibility(View.VISIBLE);
        setting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPopup(v);
            }
        });
    }

    private void checkPin(String s) {
        dataBaseHandler db=new dataBaseHandler(this);
        Employee employee=db.getEmp(s.toString());

        if(employee!=null && toggleButton.isChecked()==false)
        {
            //Toast.makeText(this,"3",Toast.LENGTH_LONG).show();

           wel.setText(employee.getMsg());
           editText.setEnabled(false);
           keyboard.removeinput();
         //  editText.setFocusable(false);
           name.setText(employee.getName());
           scan.setVisibility(View.GONE);
           name.setVisibility(View.VISIBLE);
           qrCodeScanner.setVisibility(View.GONE);
           Bitmap bitmap=BitmapFactory.decodeByteArray(employee.getImage(),0,employee.getImage().length);
           user.setImageBitmap(bitmap);
           user.setVisibility(View.VISIBLE);

           // editText.setText("");
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    Refresh();
                }
            },3000);


        }
        else
        Refresh();
    }


    private void showPopup(View v) {
        PopupMenu popupMenu=new PopupMenu(this,v);
        popupMenu.inflate(R.menu.menu);
        popupMenu.show();
        popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if(item.getItemId()==R.id.log)
                {
//                   intite();
//                    editor.putInt("curr",0);
//                    editor.commit();
                }
                else if(item.getItemId()==R.id.reg)
                    startActivity(new Intent(Home.this,Registration.class));
              //  Toast.makeText(getApplicationContext(),item.getTitle()+" is clicked",Toast.LENGTH_LONG).show();
                return true;
            }
        });
    }
    private void time()
    {
        final DateFormat timFormat=new SimpleDateFormat("HH:mm");
        tim.setText(timFormat.format(new Date()));
        DateFormat datFormat=new SimpleDateFormat("dd/MM/yyyy  EEE ");
        dat.setText(datFormat.format(new Date()));
    Handler handler=    new Handler(getMainLooper());
    handler.postDelayed(new Runnable() {
        @Override
        public void run() {
            time();
        }
    },1000*60);
    }

    @Override
    protected void onStart() {
        super.onStart();
//        if(sharedPreferences.getInt("curr",0)==0)
        //  intite();
       // editText.setText("");
    }
    private void setScannerProperties() {
        qrCodeScanner.setFormats(ZXingScannerView.ALL_FORMATS);
        qrCodeScanner.setAutoFocus(true);
        qrCodeScanner.setLaserColor(R.color.colorAccent);
        qrCodeScanner.setMaskColor(R.color.colorAccent);

    }
    @Override
    protected void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(Home.this, Manifest.permission.CAMERA)
                ==PackageManager.PERMISSION_GRANTED) {
            qrCodeScanner.startCamera();
            qrCodeScanner.setResultHandler(this);

        }
       // if(sharedPreferences.getInt("curr",0)==0)
          //  intite();
    }

    @Override
    protected void onPause() {
        super.onPause();
       // if (qrCodeScanner!=null)
        //qrCodeScanner.stopCamera();
    }
    @Override
    public void handleResult(Result result) {
        checkPin(result.getText());
        qrCodeScanner.startCamera();
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case 200: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    ImageView qr=(ImageView)findViewById(R.id.qr);
                    qr.setImageResource(R.drawable.qr_active);
                    qrCodeScanner.setVisibility(View.VISIBLE);
                    // qrCodeScanner =(ZXingScannerView) findViewById(R.id.qrCodeScanner);
                    setScannerProperties();

                } else {
                    Toast.makeText(Home.this, "you cannot scan qr  ", Toast.LENGTH_LONG).show();
                }
            }
        }
    }
}
