package com.example.zas.keyboard.Objects;

public class Employee {
    String Name,Pin,Mail,Birthday,Addresse,Mobile,msg;
    int Role,Department,Gender,id;
    byte[]Image;

    public Employee(String mobile, String name, String pin, int gender, int role,
                    String mail, String birthday, int department, String addresse, byte[] image,int id) {
        Name = name;
        Pin = pin;
        Mail = mail;
        Birthday = birthday;
        Addresse = addresse;
        Mobile = mobile;
        Role = role;
        Department = department;
        Gender = gender;
        this.id = id;
        Image = image;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Employee(int id, byte[] image, String name, String Msg) {
        Image = image;
        this.id=id;
        Name=name;
        msg=Msg;
    }

    public String getMsg() {
        return msg;
    }

    public int getId() {
        return id;
    }

    public Employee(String mobile, String name, String pin, int gender, int role,
                    String mail, String birthday, int department, String addresse, byte[] image) {
        Name = name;
        Mobile=mobile;
        Pin = pin;
        Gender = gender;
        Role = role;
        Mail = mail;
        Birthday = birthday;
        Department = department;
        Addresse = addresse;
        Image = image;
    }

    public String getName() {
        return Name;
    }

    public String getMobile() {
        return Mobile;
    }

    public String getPin() {
        return Pin;
    }

    public int getGender() {
        return Gender;
    }

    public int getRole() {
        return Role;
    }

    public String getMail() {
        return Mail;
    }

    public String getBirthday() {
        return Birthday;
    }

    public int getDepartment() {
        return Department;
    }

    public String getAddresse() {
        return Addresse;
    }

    public byte[] getImage() {
        return Image;
    }
}
