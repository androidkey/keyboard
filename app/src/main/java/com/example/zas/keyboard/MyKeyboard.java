package com.example.zas.keyboard;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.text.TextUtils;
import android.util.AttributeSet;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputConnection;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

public class MyKeyboard extends LinearLayout implements View.OnClickListener {

    private ImageView button1, button2, button3, button4,
            button5, button6, button7, button8,
            button9, button0, buttonDelete, buttonNext;

    private SparseArray<String> keyValues = new SparseArray<>();
    private InputConnection inputConnection;

    public MyKeyboard(Context context) {
        this(context, null, 0);
    }

    public MyKeyboard(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public MyKeyboard(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        LayoutInflater.from(context).inflate(R.layout.keyboard, this, true);
        button1 = (ImageView) findViewById(R.id.button_1);
        button1.setOnClickListener(this);
        button2 = (ImageView) findViewById(R.id.button_2);
        button2.setOnClickListener(this);
        button3 = (ImageView) findViewById(R.id.button_3);
        button3.setOnClickListener(this);
        button4 = (ImageView) findViewById(R.id.button_4);
        button4.setOnClickListener(this);
        button5 = (ImageView) findViewById(R.id.button_5);
        button5.setOnClickListener(this);
        button6 = (ImageView) findViewById(R.id.button_6);
        button6.setOnClickListener(this);
        button7 = (ImageView) findViewById(R.id.button_7);
        button7.setOnClickListener(this);
        button8 = (ImageView) findViewById(R.id.button_8);
        button8.setOnClickListener(this);
        button9 = (ImageView) findViewById(R.id.button_9);
        button9.setOnClickListener(this);
        button0 = (ImageView) findViewById(R.id.button_0);
        button0.setOnClickListener(this);
        buttonDelete = (ImageView) findViewById(R.id.button_delete);
        buttonDelete.setOnClickListener(this);
        buttonNext = (ImageView) findViewById(R.id.button_enter);
        buttonNext.setOnClickListener(this);

        keyValues.put(R.id.button_1, "1");
        keyValues.put(R.id.button_2, "2");
        keyValues.put(R.id.button_3, "3");
        keyValues.put(R.id.button_4, "4");
        keyValues.put(R.id.button_5, "5");
        keyValues.put(R.id.button_6, "6");
        keyValues.put(R.id.button_7, "7");
        keyValues.put(R.id.button_8, "8");
        keyValues.put(R.id.button_9, "9");
        keyValues.put(R.id.button_0, "0");

    }

    @Override
    public void onClick(View view) {
        if (inputConnection == null)
            return;

        if (view.getId() == R.id.button_delete) {
            CharSequence selectedText = inputConnection.getSelectedText(0);

            if (TextUtils.isEmpty(selectedText)) {
                inputConnection.deleteSurroundingText(1, 0);
            } else {
                inputConnection.commitText("", 1);
            }
        }
      else   if (view.getId() == R.id.button_enter) {
            Toast.makeText(getContext(),"Invalid Pin ,try again ",Toast.LENGTH_LONG).show();

        }
            else
         {
            String value = keyValues.get(view.getId());
            inputConnection.commitText(value, 1);
        }
    }

    public void setInputConnection(InputConnection ic) {
        inputConnection = ic;
    }
    public void removeinput()
    {
        inputConnection=null;
    }
}