package com.example.zas.keyboard;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.example.zas.keyboard.Adapters.spinnerAdapter;
import com.example.zas.keyboard.R;

import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zas.keyboard.Adapters.staffAdapter;
import com.example.zas.keyboard.Objects.Employee;
import com.example.zas.keyboard.Objects.roleDeptGender;

import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.jar.Attributes;

public class Registration extends AppCompatActivity {

    boolean img=false;
    byte []Image;
    ImageView image;
    RecyclerView recyclerView;
android.support.v7.widget.SearchView searchView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN
                , WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(com.example.zas.keyboard.R.layout.activity_registration);
        inite();
    }
    public void inite()
    {
        searchView=(android.support.v7.widget.SearchView) findViewById(com.example.zas.keyboard.R.id.search);
     //  searchView.onActionViewExpanded();

        FloatingActionButton floatingActionButton=(FloatingActionButton)findViewById(com.example.zas.keyboard.R.id.add);
        recyclerView =(RecyclerView)findViewById(com.example.zas.keyboard.R.id.rec);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this, LinearLayout.VERTICAL,false));
        dataBaseHandler db=new dataBaseHandler(this);
        ArrayList<Employee>employees=db.getEmployees();
        db.close();
        recyclerView.setAdapter(new staffAdapter(employees,this));
        searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                Toast.makeText(Registration.this,"welcome",Toast.LENGTH_LONG).show();
            }
        });
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addEmployee();
            }
        });

    }
    private void addEmployee()
    {
        AlertDialog.Builder addDialouge=new AlertDialog.Builder(this);
        final LayoutInflater layoutInflater=getLayoutInflater();
        View dialogueView=(View)layoutInflater.inflate(com.example.zas.keyboard.R.layout.add_employee,null);
        addDialouge.setView(dialogueView);
        final AlertDialog a=addDialouge.create();
        a.show();
        DisplayMetrics displayMetrics=new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final WindowManager.LayoutParams layoutParams=new WindowManager.LayoutParams();
        layoutParams.width= (int) (displayMetrics.widthPixels*0.85f);
        layoutParams.height= (int) (displayMetrics.heightPixels*0.9f);
        final ArrayList<roleDeptGender>Genders,Roles,Departments;
        final dataBaseHandler db=new dataBaseHandler(this);
        Genders=db.getGenders();
        Roles=db.getRoles();
        Departments=db.getDepartments();
        db.close();
        final int[] dId = {0};
        final int[] rId = { 0 };
        final int[] gId = {0};
        final String Rolelist[]=new String[(Roles.size())];
        int i=0;
        while (i<Roles.size())
        {
            Rolelist[i]=Roles.get((i)).getName();
            i++;
        }
        final String Genderlist[]=new String[(Genders.size())];
         i=0;

        while (i<Genders.size())
        {

            Genderlist[i]=Genders.get((i)).getName();
            i++;
        }
        final String Deptlist[]=new String[Departments.size()];
        i=0;
        while (i<Departments.size())
        {
            Deptlist[i]=Departments.get((i)).getName();
            i++;
        }
        final SharedPreferences sharedPreferences=getSharedPreferences("finger",MODE_PRIVATE);
      final int  empId=sharedPreferences.getInt("lstid",0)+1;
        final int  emppin=sharedPreferences.getInt("lstpin",0)+1;
        final String pin =  String.format("%04d", emppin);
        final TextView Pin=(TextView)dialogueView.findViewById(com.example.zas.keyboard.R.id.pin);
        Pin.setText("Pin: "+pin);

final Spinner roleTxt=(Spinner) dialogueView. findViewById(com.example.zas.keyboard.R.id.role);
        spinnerAdapter adapterrole = new spinnerAdapter(Registration.this, android.R.layout.simple_list_item_1);
        adapterrole.addAll(Rolelist);
        adapterrole.add("Role");
        roleTxt.setAdapter(adapterrole);
        roleTxt.setSelection(adapterrole.getCount());
        roleTxt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(roleTxt.getSelectedItem().equals("Role"));
                else
                    rId[0] =Roles.get(position).getId();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        final Spinner deptTxt=(Spinner) dialogueView. findViewById(com.example.zas.keyboard.R.id.dept);
        spinnerAdapter adapterdept = new spinnerAdapter(Registration.this, android.R.layout.simple_list_item_1);
        adapterdept.addAll(Deptlist);
        adapterdept.add("Department");
        deptTxt.setAdapter(adapterdept);
        deptTxt.setSelection(adapterdept.getCount());
        deptTxt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(deptTxt.getSelectedItem().equals("Department"))
                {

                }
             else
                 dId[0]=Departments.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        final Spinner genderTxt=(Spinner) dialogueView. findViewById(com.example.zas.keyboard.R.id.gender);
        spinnerAdapter adapterGender = new spinnerAdapter(Registration.this, android.R.layout.simple_list_item_1);
        adapterGender.addAll(Genderlist);
        adapterGender.add("Gender");
        genderTxt.setAdapter(adapterGender);
        genderTxt.setSelection(adapterGender.getCount());
        genderTxt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override

            public void onItemSelected(AdapterView<?> parent, View view,

                                       int position, long id) {

// TODO Auto-generated method stub

                if(genderTxt.getSelectedItem() == "Gender")

                {

//Do nothing.

                }

                else{

                  // Toast.makeText(Registration.this, ""+position, Toast.LENGTH_LONG).show();

                    gId[0] =Genders.get(position).getId();
                }

            }

            @Override

            public void onNothingSelected(AdapterView<?> parent) {

// TODO Auto-generated method stub

            }

        });
        image=(ImageView)dialogueView.findViewById(com.example.zas.keyboard.R.id.img);
        final TextView birthDay=(TextView)dialogueView.findViewById(com.example.zas.keyboard.R.id.birth);
        birthDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view=layoutInflater.inflate(com.example.zas.keyboard.R.layout.calendar,null);
                final PopupWindow popupWindow=new PopupWindow(view, WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.WRAP_CONTENT,true);
                popupWindow.showAtLocation(birthDay, Gravity.LEFT,0,0);
                CalendarView calendar=(CalendarView) view.findViewById(com.example.zas.keyboard.R.id.calendarbirth);
                calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                        birthDay.setText(""+year+"-"+month+"-"+dayOfMonth);
                        popupWindow.dismiss();
                        //Toast.makeText(Registration.this,""+year+"-"+month+"-"+dayOfMonth,Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
      ImageView floatingActionButton=(ImageView) dialogueView.findViewById(com.example.zas.keyboard.R.id.take_picture);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent,"Select Profile Image"),1);
            }
        });
        RelativeLayout done,cancel;
        done=(RelativeLayout)dialogueView.findViewById(com.example.zas.keyboard.R.id.done);
        cancel=(RelativeLayout)dialogueView.findViewById(com.example.zas.keyboard.R.id.cancel);
cancel.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        a.cancel();
    }
});
        final EditText Name=(EditText)dialogueView.findViewById(com.example.zas.keyboard.R.id.name),
                Mail=(EditText)dialogueView.findViewById(com.example.zas.keyboard.R.id.email),
                Phone=(EditText)dialogueView.findViewById(com.example.zas.keyboard.R.id.phone),
                Addresse=(EditText)dialogueView.findViewById(com.example.zas.keyboard.R.id.Addresse);
done.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        String name= Name.getText().toString();
        String birth=birthDay.getText().toString();
        String phone=Phone.getText().toString();
        String mail=Mail.getText().toString();
        String adreese=Addresse.getText().toString();
        if(name==null || name.length()<=0)
            Name.setError("You should Enter name");
        else if(gId[0]==0)
            Toast.makeText(Registration.this,"You should choose gender",Toast.LENGTH_LONG).show();
        else if(rId[0]==0)
            Toast.makeText(Registration.this,"You should choose role",Toast.LENGTH_LONG).show();
        else if(dId[0]==0)
            Toast.makeText(Registration.this,"You should choose department",Toast.LENGTH_LONG).show();
        else if(birth.length()<=0)
            birthDay.setError("You should choose birthday");
        else if(phone==null || phone.length()<=0)
            Phone.setError("You should enter phone Number");
        else if(mail==null || mail.length()<=0)
            Mail.setError("You should enter valid mail");
        else if(adreese==null || adreese.length()<=0)
        Addresse.setError("You should enter  adresse");
        else {
            Drawable drawable=image.getDrawable();
            BitmapDrawable bitmapDrawable=((BitmapDrawable)drawable);
            Bitmap bitmap=bitmapDrawable.getBitmap();
            ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
            Image=byteArrayOutputStream.toByteArray();
            dataBaseHandler db=new dataBaseHandler(Registration.this);
            db.addEmployee(new Employee(phone,name,pin, gId[0], rId[0],mail,birth, dId[0],adreese,Image,empId));
            db.close();
            a.cancel();
            SharedPreferences.Editor editor=sharedPreferences.edit();
            editor.putInt("lstid",(empId));
            editor.putInt("lstpin",(emppin));
            editor.commit();
            refresh();
            a.cancel();
        }
    }
});
        a.getWindow().setAttributes(layoutParams);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1)
        {
            Uri selectedUri=data.getData();
            try {
                InputStream imgstream=getContentResolver().openInputStream(selectedUri);
                Bitmap bitmap=BitmapFactory.decodeStream(imgstream);
                image.setImageBitmap(bitmap);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

        }
    }
    public void refresh()
    {
        dataBaseHandler db=new dataBaseHandler(this);
        ArrayList<Employee>employees=db.getEmployees();
        db.close();
        recyclerView.setAdapter(new staffAdapter(employees,this));
    }
    public void edit(final Employee employee)
    {
        AlertDialog.Builder addDialouge=new AlertDialog.Builder(Registration.this);
        final LayoutInflater layoutInflater=Registration.this.getLayoutInflater();
        View dialogueView=(View)layoutInflater.inflate(R.layout.add_employee,null);
        addDialouge.setView(dialogueView);
        final AlertDialog a=addDialouge.create();
        a.show();
        DisplayMetrics displayMetrics=new DisplayMetrics();
        Registration.this. getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final WindowManager.LayoutParams layoutParams=new WindowManager.LayoutParams();
        layoutParams.width= (int) (displayMetrics.widthPixels*0.85f);
        layoutParams.height= (int) (displayMetrics.heightPixels*0.9f);
        final ArrayList<roleDeptGender>Genders,Roles,Departments;
        final dataBaseHandler db=new dataBaseHandler(Registration.this);
        Genders=db.getGenders();
        Roles=db.getRoles();
        Departments=db.getDepartments();
        db.close();
        final int[] dId = {employee.getDepartment()};
        final int[] rId = { employee.getRole() };
        final int[] gId = {employee.getGender()};
        final String Rolelist[]=new String[(Roles.size())];
        int i=0;
        int selectedR=0;
        while (i<Roles.size())
        {
            Rolelist[i]=Roles.get((i)).getName();
            if(employee.getRole()==Roles.get(i).getId())
                selectedR=i;
            i++;
        }
        final String Genderlist[]=new String[(Genders.size())];
        i=0;
        int selectedG=0;
        while (i<Genders.size())
        {

            Genderlist[i]=Genders.get((i)).getName();
            if(Genders.get(i).getId()==employee.getGender())
                selectedG=i;
            i++;
        }
        final String Deptlist[]=new String[Departments.size()];
        i=0;
        int selectedD=0;
        while (i<Departments.size())
        {
            Deptlist[i]=Departments.get((i)).getName();
            if(Departments.get(i).getId()==employee.getDepartment())
                selectedD=i;
            i++;
        }

        final String pin =  employee.getPin();
        final TextView Pin=(TextView)dialogueView.findViewById(R.id.pin);
        Pin.setText("Pin: "+pin);

        final Spinner roleTxt=(Spinner) dialogueView. findViewById(com.example.zas.keyboard.R.id.role);
        spinnerAdapter adapterrole = new spinnerAdapter(Registration.this, android.R.layout.simple_list_item_1);
        adapterrole.addAll(Rolelist);
        adapterrole.add("");
        roleTxt.setAdapter(adapterrole);
        roleTxt.setSelection(selectedR);
        //roleTxt.setSelection(adapterrole.getCount());
        roleTxt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                rId[0] =Roles.get(position).getId();
              //  Toast.makeText(Registration.this,"role"+rId[0], Toast.LENGTH_LONG).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        final Spinner deptTxt=(Spinner) dialogueView. findViewById(com.example.zas.keyboard.R.id.dept);
        spinnerAdapter adapterdept = new spinnerAdapter(Registration.this, android.R.layout.simple_list_item_1);
        adapterdept.addAll(Deptlist);
        adapterdept.add("");
        deptTxt.setAdapter(adapterdept);
        deptTxt.setSelection(selectedD);
        //deptTxt.setSelection(adapterdept.getCount());
        deptTxt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                dId[0]=Departments.get(position).getId();
               // Toast.makeText(Registration.this,"dept"+dId[0], Toast.LENGTH_LONG).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        final Spinner genderTxt=(Spinner) dialogueView. findViewById(com.example.zas.keyboard.R.id.gender);
        spinnerAdapter adapterGender = new spinnerAdapter(Registration.this, android.R.layout.simple_list_item_1);
        adapterGender.addAll(Genderlist);
        adapterGender.add("");
        genderTxt.setAdapter(adapterGender);
        genderTxt.setSelection(selectedG);
       // genderTxt.setSelection(adapterGender.getCount());
        genderTxt.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override

            public void onItemSelected(AdapterView<?> parent, View view,

                                       int position, long id) {

// TODO Auto-generated method stub

                if(genderTxt.getSelectedItem() == "This is Hint Text")

                {

//Do nothing.

                }

                else{



                    gId[0] =Genders.get(position).getId();
                   // Toast.makeText(Registration.this,"Gender"+gId[0], Toast.LENGTH_LONG).show();
                }

            }

            @Override

            public void onNothingSelected(AdapterView<?> parent) {

// TODO Auto-generated method stub

            }

        });
        image=(ImageView)dialogueView.findViewById(R.id.img);
        Bitmap bitmap=BitmapFactory.decodeByteArray(employee.getImage(),0,employee.getImage().length);
        image.setImageBitmap(bitmap);
        final TextView birthDay=(TextView)dialogueView.findViewById(R.id.birth);
        birthDay.setText(employee.getBirthday());
        birthDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view=layoutInflater.inflate(R.layout.calendar,null);
                final PopupWindow popupWindow=new PopupWindow(view, WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.WRAP_CONTENT,true);
                popupWindow.showAtLocation(birthDay, Gravity.LEFT,0,0);
                CalendarView calendar=(CalendarView) view.findViewById(R.id.calendarbirth);
                calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                        birthDay.setText(""+year+"-"+month+"-"+dayOfMonth);
                        popupWindow.dismiss();
                        //Toast.makeText(Registration.this,""+year+"-"+month+"-"+dayOfMonth,Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        ImageView floatingActionButton=(ImageView) dialogueView.findViewById(R.id.take_picture);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                 startActivityForResult(Intent.createChooser(intent,"Select Profile Image"),1);
            }
        });
        RelativeLayout done,cancel;
        done=(RelativeLayout)dialogueView.findViewById(R.id.done);
        cancel=(RelativeLayout)dialogueView.findViewById(R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                a.cancel();
            }
        });
        final EditText Name=(EditText)dialogueView.findViewById(R.id.name),
                Mail=(EditText)dialogueView.findViewById(R.id.email),
                Phone=(EditText)dialogueView.findViewById(R.id.phone),
                Addresse=(EditText)dialogueView.findViewById(R.id.Addresse);
        Name.setText(employee.getName());
        Mail.setText(employee.getMail());
        Addresse.setText(employee.getAddresse());
        Phone.setText(employee.getMobile());
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name= Name.getText().toString();
                String birth=birthDay.getText().toString();
                String phone=Phone.getText().toString();
                String mail=Mail.getText().toString();
                String adreese=Addresse.getText().toString();
                if(name==null || name.length()<=0)
                    Name.setError("You should Enter name");
                else if(gId[0]==0)
                    Toast.makeText(Registration.this,"You should choose gender",Toast.LENGTH_LONG).show();
                else if(rId[0]==0)
                    Toast.makeText(Registration.this,"You should choose role",Toast.LENGTH_LONG).show();
                else if(dId[0]==0)
                   Toast.makeText(Registration.this,"You should choose department",Toast.LENGTH_LONG).show();
                else if(birth.length()<=0)
                    birthDay.setError("You should choose birthday");
                else if(phone==null || phone.length()<=0)
                    Phone.setError("You should enter phone Number");
                else if(mail==null || mail.length()<=0)
                    Mail.setError("You should enter valid mail");
                else if(adreese==null || adreese.length()<=0)
                    Addresse.setError("You should enter  adresse");
                else {
                    Drawable drawable=image.getDrawable();
                    BitmapDrawable bitmapDrawable=((BitmapDrawable)drawable);
                    Bitmap bitmap=bitmapDrawable.getBitmap();
                    ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
                    byte[]   Image=byteArrayOutputStream.toByteArray();
                    dataBaseHandler db=new dataBaseHandler(Registration.this);
                       db.updateEmpolyee(new Employee(phone,name,pin, gId[0], rId[0],mail,birth, dId[0],adreese,Image,employee.getId()));
                    db.close();
                    refresh();
                    a.cancel();

                    //  refresh();
                    a.cancel();
                }
            }
        });
        a.getWindow().setAttributes(layoutParams);

    }
}
