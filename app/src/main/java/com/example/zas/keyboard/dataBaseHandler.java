package com.example.zas.keyboard;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.example.zas.keyboard.Objects.Employee;
import com.example.zas.keyboard.Objects.roleDeptGender;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class dataBaseHandler extends SQLiteOpenHelper {
   private final static  String DBName="AttendanceDB";
   Context context;
   private final static int DBVersion=2;
   private final static String genderTable="Gender";
    private final static String departmentTable="Department";
    private final static String roleTable="Role";
    private final static String attendanceTable="Attendance";
    private final static String employeeTable="Employee";
    private final static String idDepartment="dept_id";
    private final static String idRole="role_id";
    private final static String idEmployee="emp_id";
    private final static String idattendance="attend_id";
    private final static String operationAttenadance="operation";
    private final static String attendDate="date";
    private final static String attendTime="time";
    private final static String idGender="gender_id";
    private final static String Name="name";
    private final static String empAddresse="addresse";
    private final static String empImg="Img";
    private final static String empPin="pin";
    private final static String empBirthDay="birthday";
    private final static String empMail="mail";
    private final static String empMobile="mobile";
    private final static String dropQuery="DROP TABLE IF EXISTS ";
    private final static String creatEmpTable="CREATE TABLE "+employeeTable+"("+idEmployee+" INTEGER PRIMARY KEY  NOT NULL,"+
            Name+ " TEXT,"+empImg+" BLOB,"+empPin+" TEXT,"+empAddresse+" TEXT,"
            +empMail+" TEXT,"+empBirthDay+" TEXT,"+empMobile+" TEXT,"
            +idDepartment+" INTEGER,"+idGender+" INTEGER,"+idRole+" INTEGER)";
    private final static String creatRoleTable="CREATE TABLE "+roleTable+"("+idRole+" INTEGER PRIMARY KEY  NOT NULL,"+
            Name+ " TEXT)";
    private final static String creatAttendanceTable="CREATE TABLE "+attendanceTable+
            "("+idattendance+" INTEGER PRIMARY KEY AUTOINCREMENT  NOT NULL,"+ idEmployee+" INTEGER,"
            +attendDate+" TEXT,"+attendTime+" TEXT,"+
         operationAttenadance+ " TEXT)";
 private final static String creatDeptTable="CREATE TABLE "+departmentTable+"("+idDepartment+" INTEGER PRIMARY KEY  NOT NULL,"+
         Name+ " TEXT)";
 private final static String creatGenderTable="CREATE TABLE "+genderTable+"("+idGender+" INTEGER PRIMARY KEY  NOT NULL,"+
         Name+ " TEXT)";
    public dataBaseHandler(Context context) {

        super(context, DBName, null, DBVersion);
     this.context=context;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
db.execSQL(creatEmpTable);
db.execSQL(creatDeptTable);
db.execSQL(creatGenderTable);
db.execSQL(creatRoleTable);
db.execSQL(creatAttendanceTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
     db.execSQL(dropQuery+employeeTable);
     db.execSQL(dropQuery+genderTable);
     db.execSQL(dropQuery+roleTable);
     db.execSQL(dropQuery+departmentTable);
     db.execSQL(dropQuery+attendanceTable);
    }
    public void addEmployee(Employee employee)
    {
     SQLiteDatabase db=this.getReadableDatabase();
     ContentValues contentValues=new ContentValues();
     contentValues.put(Name,employee.getName());
     contentValues.put(empAddresse,employee.getAddresse());
     contentValues.put(empBirthDay,employee.getBirthday());
     contentValues.put(empImg,employee.getImage());
     contentValues.put(idRole,employee.getRole());
     contentValues.put(idDepartment,employee.getDepartment());
     contentValues.put(empMail,employee.getMail());
     contentValues.put(empPin,employee.getPin());
     contentValues.put(empMobile,employee.getMobile());
     contentValues.put(idGender,employee.getGender());
     contentValues.put(idEmployee,employee.getId());
   long i=  db.insert(employeeTable,null,contentValues);
    // Toast.makeText(context,""+i,Toast.LENGTH_LONG).show();
     db.close();
    }
    public ArrayList<Employee>getEmployees()
    {
     ArrayList<Employee>employees=new ArrayList<Employee>();
     String query="SELECT * FROM "+employeeTable;
     SQLiteDatabase db=this.getWritableDatabase();
     Cursor cursor=db.rawQuery(query,null);
     if(cursor.moveToFirst())
     {
      do {
       Employee employee=new Employee(cursor.getString(cursor.getColumnIndex(empMobile)),cursor.getString(cursor.getColumnIndex(Name))
               ,cursor.getString(cursor.getColumnIndex(empPin)), cursor.getInt(cursor.getColumnIndex(idGender)),
               cursor.getInt(cursor.getColumnIndex(idRole)),cursor.getString(cursor.getColumnIndex(empMail)),
               cursor.getString(cursor.getColumnIndex(empBirthDay)),cursor.getInt(cursor.getColumnIndex(idDepartment)),
               cursor.getString(cursor.getColumnIndex(empAddresse)),cursor.getBlob(cursor.getColumnIndex(empImg)),cursor.getInt(cursor.getColumnIndex(idEmployee)));
       employees.add(employee);

       }
       while (cursor.moveToNext());
      }

     return employees;
    }
    public void  deleteEmployee(int id)
    {
     SQLiteDatabase db=this.getWritableDatabase();
     db.delete(employeeTable,idEmployee+" = ?",new String[]{ String.valueOf(id)});
     db.close();
    }
    public void addRole(roleDeptGender role)
    {
     SQLiteDatabase db=this.getReadableDatabase();
     ContentValues contentValues=new ContentValues();
     contentValues.put(idRole,role.getId());
     contentValues.put(Name,role.getName());
     db.insert(roleTable,null,contentValues);
    }
 public void addDepartment(roleDeptGender dept)
 {
  SQLiteDatabase db=this.getReadableDatabase();
  ContentValues contentValues=new ContentValues();
  contentValues.put(idDepartment,dept.getId());
  contentValues.put(Name,dept.getName());
  db.insert(departmentTable,null,contentValues);
 }
 public void addGender(roleDeptGender gender)
 {
  SQLiteDatabase db=this.getReadableDatabase();
  ContentValues contentValues=new ContentValues();
  contentValues.put(idGender,gender.getId());
  contentValues.put(Name,gender.getName());
  db.insert(genderTable,null,contentValues);
 }
 public ArrayList<roleDeptGender> getGenders()
 {
  ArrayList<roleDeptGender>genders=new ArrayList<roleDeptGender>();
  String query="SELECT * FROM "+genderTable;
  SQLiteDatabase db=this.getWritableDatabase();
  Cursor cursor=db.rawQuery(query,null);
  if(cursor.moveToFirst()) {
   do {
    roleDeptGender gender=new roleDeptGender(cursor.getInt(cursor.getColumnIndex(idGender)),cursor.getString(cursor.getColumnIndex(Name)));
    genders.add(gender);
   }
   while (cursor.moveToNext());
  }
  return  genders;
 }
 public ArrayList<roleDeptGender> getDepartments()
 {
  ArrayList<roleDeptGender>departments=new ArrayList<roleDeptGender>();
  String query="SELECT * FROM "+departmentTable;
  SQLiteDatabase db=this.getWritableDatabase();
  Cursor cursor=db.rawQuery(query,null);
  if(cursor.moveToFirst()) {
   do {
    roleDeptGender dept=new roleDeptGender(cursor.getInt(cursor.getColumnIndex(idDepartment)),cursor.getString(cursor.getColumnIndex(Name)));
    departments.add(dept);
   }
   while (cursor.moveToNext());
  }
  return  departments;
 }
 public ArrayList<roleDeptGender> getRoles()
 {
  ArrayList<roleDeptGender>roles=new ArrayList<roleDeptGender>();
  String query="SELECT * FROM "+roleTable;
  SQLiteDatabase db=this.getWritableDatabase();
  Cursor cursor=db.rawQuery(query,null);
  if(cursor.moveToFirst()) {
   do {
    roleDeptGender role=new roleDeptGender(cursor.getInt(cursor.getColumnIndex(idRole)),cursor.getString(cursor.getColumnIndex(Name)));
    roles.add(role);
   }
   while (cursor.moveToNext());
  }
  return  roles;
 }
 public Employee getEmp(String Pin)
 {
  //Toast.makeText(context,Pin,Toast.LENGTH_LONG).show();
  Employee employee=null;
  String query="SELECT "+idEmployee+", "+empImg+", "+Name+" FROM "+employeeTable+" where "+empPin+"="+Pin;
  SQLiteDatabase db=this.getWritableDatabase();
  Cursor cursor=db.rawQuery(query,null);
  if(cursor.moveToFirst())
  {
      int id=cursor.getInt(cursor.getColumnIndex(idEmployee));
      Date date= Calendar.getInstance().getTime();
    DateFormat format=new SimpleDateFormat("dd-MM-yyyy");
    DateFormat tim=new SimpleDateFormat("HH:mm");
      String Operation=checkAttendance(format.format(date),id);
     // Toast.makeText(context,""+Operation,Toast.LENGTH_LONG).show();
if(Operation.equals("OUT")==true)
    Toast.makeText(context,"You Already checkin/out today ",Toast.LENGTH_LONG).show();
else {
    if(Operation.equals("IN")==true) {
        Operation = "Good Bye";
        add_Attendance("OUT",id,format.format(date),tim.format(date));
    }
    else {
        Operation = "Welcome";
        add_Attendance("IN",id,format.format(date),tim.format(date));
    }
   employee=new Employee(id,cursor.getBlob(cursor.getColumnIndex(empImg)),cursor.getString(cursor.getColumnIndex(Name)),Operation);
  }}
  else
      Toast.makeText(context,"Invalid Pin/QR ,try again ",Toast.LENGTH_LONG).show();
  return employee;
 }
public void updateEmpolyee(Employee employee)
{
 SQLiteDatabase db=this.getReadableDatabase();
 ContentValues contentValues=new ContentValues();
 contentValues.put(Name,employee.getName());
 contentValues.put(empAddresse,employee.getAddresse());
 contentValues.put(empBirthDay,employee.getBirthday());
 contentValues.put(empImg,employee.getImage());
 contentValues.put(idRole,employee.getRole());
 contentValues.put(idDepartment,employee.getDepartment());
 contentValues.put(empMail,employee.getMail());
 contentValues.put(empPin,employee.getPin());
 contentValues.put(empMobile,employee.getMobile());
 contentValues.put(idGender,employee.getGender());
 contentValues.put(idEmployee,employee.getId());
 db.update(employeeTable,contentValues,""+idEmployee+"="+employee.getId(),null);
 // Toast.makeText(context,""+i,Toast.LENGTH_LONG).show();
 db.close();
}
private String checkAttendance(String date,int id)
{
    String operation="";
    String query="SELECT "+operationAttenadance+","+attendDate+" FROM "+attendanceTable+" where "+idEmployee+"="+id
            +" ORDER BY "+ idattendance+" DESC LIMIT 1";
    SQLiteDatabase db=this.getWritableDatabase();
    Cursor cursor=db.rawQuery(query,null);
    if(cursor.moveToFirst())
    {
        if(date.equals(cursor.getString(cursor.getColumnIndex(attendDate))))
            operation=cursor.getString(cursor.getColumnIndex(operationAttenadance)) ;
       //Toast.makeText(context,"wel"+cursor.getCount(),Toast.LENGTH_LONG).show();
    }
    return operation;
}
private void add_Attendance(String Operation,int empId,String date,String tim)
{
   // Toast.makeText(context,"test---"+Operation,Toast.LENGTH_LONG).show();
    SQLiteDatabase db=this.getReadableDatabase();
    ContentValues contentValues=new ContentValues();
    contentValues.put(idEmployee,empId);
    contentValues.put(operationAttenadance,Operation);
    contentValues.put(attendDate,date);
    contentValues.put(attendTime,tim);
    db.insert(attendanceTable,null,contentValues);
    //Toast.makeText(context,Operation+i,Toast.LENGTH_LONG).show();
}
}
