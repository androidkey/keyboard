package com.example.zas.keyboard.Objects;

public class roleDeptGender {
    int id;
    String Name;

    public roleDeptGender(int id, String name) {
        this.id = id;
        Name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return Name;
    }

}
