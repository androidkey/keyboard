package com.example.zas.keyboard.Adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.PopupMenu;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.zas.keyboard.Objects.Employee;
import com.example.zas.keyboard.Objects.roleDeptGender;
import com.example.zas.keyboard.R;
import com.example.zas.keyboard.Registration;
import com.example.zas.keyboard.dataBaseHandler;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.ArrayList;

public class staffAdapter extends RecyclerView.Adapter<staffAdapter.viewHolder>  {
    ArrayList<Employee>employees;
    Registration context;
ImageView image;
    public staffAdapter(ArrayList<Employee> employees, Registration context) {
        this.employees = employees;
        this.context = context;
    }

    @NonNull
    @Override
    public viewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        LayoutInflater layoutInflater=LayoutInflater.from(context);
        View view=layoutInflater.inflate(R.layout.emp_item,parent,false);
        return new viewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull viewHolder holder, final int position) {
        holder.name.setText(employees.get(position).getName());
        holder.id.setText(""+String.format("%04d",employees.get(position).getId()));
        holder.erase.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(employees.size()>1)
                {
                    dataBaseHandler db=new dataBaseHandler(context);
                    db.deleteEmployee(employees.get(position).getId());
                    db.close();
                SharedPreferences sharedPreferences=context.getSharedPreferences("finger",context.MODE_PRIVATE);
              //  Toast.makeText(context,"id:"+sharedPreferences.getInt("curr",0)+employees.get(position).getId(),Toast.LENGTH_LONG).show();
                if(sharedPreferences.getInt("curr",0)==employees.get(position).getId())
                {
                    SharedPreferences.Editor editor=sharedPreferences.edit();
                    editor.putInt("curr",0);
                    editor.commit();
                    context.finish();
                }
                else
                    context.refresh();
                }
                else
                    Toast.makeText(context,"You cannot delete this is account as it is the only exist account",Toast.LENGTH_LONG).show();
            }
        });
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              context. edit(employees.get(position));
            }
        });

    }

    @Override
    public int getItemCount() {
        return employees.size();
    }

    public class viewHolder extends RecyclerView.ViewHolder {
        TextView name,id;
        ImageView erase,edit;
        public viewHolder(View itemView) {
            super(itemView);
            name=(TextView)itemView.findViewById(R.id.name);
            id=(TextView)itemView.findViewById(R.id.id);
            erase=(ImageView)itemView.findViewById(R.id.erase);
            edit=(ImageView)itemView.findViewById(R.id.edit);
        }
    }

    public void edit(final Employee employee)
    {
        AlertDialog.Builder addDialouge=new AlertDialog.Builder(context);
        final LayoutInflater layoutInflater=context.getLayoutInflater();
        View dialogueView=(View)layoutInflater.inflate(com.example.zas.keyboard.R.layout.add_employee,null);
        addDialouge.setView(dialogueView);
        final AlertDialog a=addDialouge.create();
        a.show();
        DisplayMetrics displayMetrics=new DisplayMetrics();
       context. getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        final WindowManager.LayoutParams layoutParams=new WindowManager.LayoutParams();
        layoutParams.width= (int) (displayMetrics.widthPixels*0.85f);
        layoutParams.height= (int) (displayMetrics.heightPixels*0.9f);
        final ArrayList<roleDeptGender>Genders,Roles,Departments;
        final dataBaseHandler db=new dataBaseHandler(context);
        Genders=db.getGenders();
        Roles=db.getRoles();
        Departments=db.getDepartments();
        db.close();
        final int[] dId = {0};
        final int[] rId = { 0 };
        final int[] gId = {0};
        final String Rolelist[]=new String[(Roles.size())];
        int i=0;
        String selectedR="";
        while (i<Roles.size())
        {
            Rolelist[i]=Roles.get((i)).getName();
            if(employee.getRole()==Roles.get(i).getId())
                selectedR=Roles.get(i).getName();
            i++;
        }
        final String Genderlist[]=new String[(Genders.size())];
        i=0;
        String selectedG="";
        while (i<Genders.size())
        {

            Genderlist[i]=Genders.get((i)).getName();
            if(Genders.get(i).getId()==employee.getGender())
                selectedG=Genders.get(i).getName();
            i++;
        }
        final String Deptlist[]=new String[Departments.size()];
        i=0;
        String selectedD="";
        while (i<Departments.size())
        {
            Deptlist[i]=Departments.get((i)).getName();
            if(Departments.get(i).getId()==employee.getDepartment())
                selectedD=Departments.get(i).getName();
            i++;
        }

        final String pin =  employee.getPin();
        final TextView Pin=(TextView)dialogueView.findViewById(com.example.zas.keyboard.R.id.pin);
        Pin.setText("Pin: "+pin);

        final TextView roleTxt=(TextView) dialogueView. findViewById(com.example.zas.keyboard.R.id.role);
        roleTxt.setText(selectedR);
        roleTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu=new PopupMenu(context,roleTxt);
                int i=0;
                while (i<Rolelist.length) {
                    popupMenu.getMenu().add(Menu.NONE,i, Menu.NONE,Rolelist[i]);
                    i++;
                }
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        roleTxt.setText(item.getTitle());
                        rId[0] =Roles.get(item.getItemId()).getId();

                        return true;
                    }
                });
            }
        });
        final TextView deptTxt=(TextView) dialogueView. findViewById(com.example.zas.keyboard.R.id.dept);
        deptTxt.setText(selectedD);
        deptTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu=new PopupMenu(context,deptTxt);
                int i=0;
                while (i<Deptlist.length) {
                    popupMenu.getMenu().add(Menu.NONE,i, Menu.NONE,Deptlist[i]);
                    i++;
                }
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        deptTxt.setText(item.getTitle());
                        dId[0] =Departments.get(item.getItemId()).getId();
                        return true;
                    }
                });
            }
        });
        final TextView genderTxt=(TextView) dialogueView. findViewById(com.example.zas.keyboard.R.id.gender);
        genderTxt.setText(selectedG);
        genderTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                PopupMenu popupMenu=new PopupMenu(context,genderTxt);
                int i=0;
                while (i<Genderlist.length) {
                    popupMenu.getMenu().add(Menu.NONE,i, Menu.NONE,Genderlist[i]);
                    i++;
                }
                popupMenu.show();
                popupMenu.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
                    @Override
                    public boolean onMenuItemClick(MenuItem item) {
                        genderTxt.setText(item.getTitle());
                        gId[0] =Genders.get(item.getItemId()).getId();

                        return true;
                    }
                });
            }
        });
        image=(ImageView)dialogueView.findViewById(com.example.zas.keyboard.R.id.img);
        Bitmap bitmap=BitmapFactory.decodeByteArray(employee.getImage(),0,employee.getImage().length);
        image.setImageBitmap(bitmap);
        final TextView birthDay=(TextView)dialogueView.findViewById(com.example.zas.keyboard.R.id.birth);
        birthDay.setText(employee.getBirthday());
        birthDay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view=layoutInflater.inflate(com.example.zas.keyboard.R.layout.calendar,null);
                final PopupWindow popupWindow=new PopupWindow(view, WindowManager.LayoutParams.WRAP_CONTENT,
                        WindowManager.LayoutParams.WRAP_CONTENT,true);
                popupWindow.showAtLocation(birthDay, Gravity.LEFT,0,0);
                CalendarView calendar=(CalendarView) view.findViewById(com.example.zas.keyboard.R.id.calendarbirth);
                calendar.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
                    @Override
                    public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                        birthDay.setText(""+year+"-"+month+"-"+dayOfMonth);
                        popupWindow.dismiss();
                        //Toast.makeText(Registration.this,""+year+"-"+month+"-"+dayOfMonth,Toast.LENGTH_LONG).show();
                    }
                });
            }
        });
        FloatingActionButton floatingActionButton=(FloatingActionButton)dialogueView.findViewById(com.example.zas.keyboard.R.id.take_picture);
        floatingActionButton.setOnClickListener(new View.OnClickListener() {
            @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP_MR1)
            @Override
            public void onClick(View v) {
                Intent intent=new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                ((Activity)context). startActivityForResult(Intent.createChooser(intent,"Select Profile Image"),1);
            }
        });
        RelativeLayout done,cancel;
        done=(RelativeLayout)dialogueView.findViewById(com.example.zas.keyboard.R.id.done);
        cancel=(RelativeLayout)dialogueView.findViewById(com.example.zas.keyboard.R.id.cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                a.cancel();
            }
        });
        final EditText Name=(EditText)dialogueView.findViewById(com.example.zas.keyboard.R.id.name),
                Mail=(EditText)dialogueView.findViewById(com.example.zas.keyboard.R.id.email),
                Phone=(EditText)dialogueView.findViewById(com.example.zas.keyboard.R.id.phone),
                Addresse=(EditText)dialogueView.findViewById(com.example.zas.keyboard.R.id.Addresse);
        Name.setText(employee.getName());
        Mail.setText(employee.getMail());
        Addresse.setText(employee.getAddresse());
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String name= Name.getText().toString();
                String birth=birthDay.getText().toString();
                String phone=Phone.getText().toString();
                String mail=Mail.getText().toString();
                String adreese=Addresse.getText().toString();
                if(name==null || name.length()<=0)
                    Name.setError("You should Enter name");
                else if(gId[0]==0)
                    genderTxt.setError("You should choose gender");
                else if(rId[0]==0)
                    roleTxt.setError("You should choose role");
                else if(dId[0]==0)
                    deptTxt.setError("You should choose department");
                else if(birth.length()<=0)
                    birthDay.setError("You should choose birthday");
                else if(phone==null || phone.length()<=0)
                    Phone.setError("You should enter phone Number");
                else if(mail==null || mail.length()<=0)
                    Mail.setError("You should enter valid mail");
                else if(adreese==null || adreese.length()<=0)
                    Addresse.setError("You should enter  adresse");
                else {
                    Drawable drawable=image.getDrawable();
                    BitmapDrawable bitmapDrawable=((BitmapDrawable)drawable);
                    Bitmap bitmap=bitmapDrawable.getBitmap();
                    ByteArrayOutputStream byteArrayOutputStream=new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.PNG,100,byteArrayOutputStream);
                 byte[]   Image=byteArrayOutputStream.toByteArray();
                    dataBaseHandler db=new dataBaseHandler(context);
              //      db.addEmployee(new Employee(phone,name,pin, gId[0], rId[0],mail,birth, dId[0],adreese,Image,employee.getId()));
                    db.close();
                    a.cancel();

                  //  refresh();
                    a.cancel();
                }
            }
        });
        a.getWindow().setAttributes(layoutParams);

    }


}
